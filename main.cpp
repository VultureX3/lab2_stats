#include <iostream>
#include <set>
#include "StatisticMultiset.cpp"

using namespace std;

int main() {
	StatisticMultiset<int> stat;
	multiset<int> m;
	for (int i = 0; i <= 5; i++)
		m.insert(i * 10);
	stat.AddNum(5);
	stat.AddNum(6);
	stat.AddNum(10);
	stat.AddNum(1);
	stat.AddNum(m);
	cout << stat.GetMax() << ' ' << stat.GetMin() << '\n' << stat.GetAvg() << '\n' << stat.GetCountUnder(7) << '\n' << stat.GetCountAbove(9) << '\n';
	stat.AddNumbersFromFile("file.txt");
	StatisticMultiset<int> test;
	test.AddNum(100);
	test.AddNum(200);
	test.AddNum(3000);
	stat.AddNums(test);
	cout << stat.GetCountAbove(2500) << '\n';
	StatisticMultiset<char*> strtest;
	strtest.AddNum("Seventeen");
	strtest.AddNum("One");
	strtest.AddNum("Two");
	strtest.AddNum("Zero");
	cout << strtest.GetMin() << ' ' << strtest.GetMax() << '\n';
	cout << strtest.GetCountUnder("hello") << ' ' << strtest.GetCountAbove("One") << '\n';
	system("pause");
	return 0;
}
