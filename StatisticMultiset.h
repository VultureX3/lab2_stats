#pragma once

#include <set>
#include <fstream>
using namespace std;

template <class T>
class StatisticMultiset {
public:
	StatisticMultiset();

	~StatisticMultiset();
	
	void AddNum(const T& val);

	void AddNum(const multiset<T>& numbers);

	void AddNumbersFromFile(const char* filename);

	void AddNums(const StatisticMultiset &a_stat_set);

	T GetMax() const;

	T GetMin() const;

	float GetAvg() const;

	int GetCountUnder(T threshold) const;

	int GetCountAbove(T threshold) const;

private:
	multiset<T> stats;
	mutable bool gmax, gmin, gavg, gcu, gca;
	mutable T cmax, cmin;
	mutable float cavg;
	mutable pair<T, int> cgcu, cgca;

	void ResetFlags() const;
};

