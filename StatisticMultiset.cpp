#include "StatisticMultiset.h"


template <class T>
StatisticMultiset<T>::StatisticMultiset() {
	ResetFlags();
};


template <class T>
StatisticMultiset<T>::~StatisticMultiset() {};


template <class T>
void StatisticMultiset<T>::ResetFlags() const {
	gmax = gmin = gavg = gcu = gca = true;
};


template <class T>
void StatisticMultiset<T>::AddNum(const T& val) {
	stats.insert(val);
	ResetFlags();
};


template <class T>
void StatisticMultiset<T>::AddNum(const multiset<T>& numbers) {
	for (auto& it : numbers)
		AddNum(it);
	ResetFlags();
};


template <class T>
void StatisticMultiset<T>::AddNumbersFromFile(const char* filename) {
	ifstream file(filename);
	int k;
	while (file >> k)
		AddNum(k);
	ResetFlags();
};



template <class T>
void StatisticMultiset<T>::AddNums(const StatisticMultiset<T> &a_stat_set) {
	AddNum(a_stat_set.stats);
	ResetFlags();
};


template <class T>
T StatisticMultiset<T>::GetMax() const {
	if (gmax) {
		cmax = *--stats.end();
		gmax = false;
	}
	return cmax;
};


template <class T>
T StatisticMultiset<T>::GetMin() const {
	if (gmin) {
		cmin = *stats.begin();
		gmin = false;
	}
	return cmin;
};


template <class T>
float StatisticMultiset<T>::GetAvg() const {
	if (gavg) {
		float sum = 0;
		for (auto& it : stats)
			sum += it;
		cavg = sum / stats.size();
		gavg = false;
	}
	return cavg;
};


template <class T>
int StatisticMultiset<T>::GetCountUnder(T threshold) const {
	if (gcu || cgcu.first != threshold) {
		int count = 0;
		for (auto& it : stats)
			if (it < threshold)
				count++;
		cgcu = make_pair(threshold, count);
		gcu = false;
	}
	return cgcu.second;
};


template <class T>
int StatisticMultiset<T>::GetCountAbove(T threshold) const {
	if (gca || cgca.first != threshold) {
		int count = 0;
		for (auto& it : stats)
			if (it > threshold)
				count++;
		cgca = make_pair(threshold, count);
		gca = false;
	}
	return cgca.second;
}
